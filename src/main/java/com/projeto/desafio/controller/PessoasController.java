package com.projeto.desafio.controller;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.desafio.entity.Pessoas;
import com.projeto.desafio.repository.PessoaRepository;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(
		allowedHeaders = "*",
		methods = { RequestMethod.GET, RequestMethod.OPTIONS },
		origins="*"
)
@RestController
@RequestMapping("/pessoa")
public class PessoasController {
  @Autowired
  private PessoaRepository repository;
  
  @RequestMapping(value = "/", method = RequestMethod.GET)
  public List<Pessoas> getAllPessoas() {
    return repository.findAll();
  }

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  public Pessoas getPessoaById(@PathVariable("id") ObjectId id) {
    return repository.findBy_id(id);
  }
  
  @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
  public void modifyPessoaById(@PathVariable("id") ObjectId id, @Valid @RequestBody Pessoas pessoa) {
	pessoa.set_id(id);
    repository.save(pessoa);
  }
  
  @RequestMapping(value = "/", method = RequestMethod.POST)
  public Pessoas createPessoa(@Valid @RequestBody Pessoas pessoa) {
	pessoa.set_id(ObjectId.get());
    repository.save(pessoa);
    return pessoa;
  }
  
  @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
  public void deletePessoa(@PathVariable ObjectId id) {
    repository.delete(repository.findBy_id(id));
  }
}