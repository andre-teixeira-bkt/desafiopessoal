package com.projeto.desafio.entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;


public class Pessoas {
  @Id
  public ObjectId _id;
  
  public int cdPessoa;
  public String firstName;
  public String lastName;
  public Object []contacDetails;
  
  // Constructors
  public Pessoas() {}
  
  public Pessoas(ObjectId _id, int cdPessoa, String firstName, String lastName, Object []contacDetails) {
    this._id = _id;
    this.cdPessoa  = cdPessoa;
    this.firstName = firstName;
    this.lastName = lastName;
    this.contacDetails = contacDetails;
  }

 public String get_id() { 
	return _id.toHexString(); 
}

public void set_id(ObjectId _id) { 
	this._id = _id;
}

public int getCdPessoa() {
	return cdPessoa;
}

public void setCdPessoa(int cdPessoa) {
	this.cdPessoa = cdPessoa;
}

public String getFirstName() {
	return firstName;
}

public void setFirstName(String firstName) {
	this.firstName = firstName;
}

public String getLastName() {
	return lastName;
}

public void setLastName(String lastName) {
	this.lastName = lastName;
}

public Object[] getContacDetails() {
	return contacDetails;
}

public void setContacDetails(Object[] contacDetails) {
	this.contacDetails = contacDetails;
}

@Override
public String toString() {
	return "Pessoa [id=" + _id + ", name= " + firstName + lastName + ", Contatos= " + contacDetails + "]";
}

}
