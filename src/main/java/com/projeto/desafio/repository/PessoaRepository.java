package com.projeto.desafio.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.projeto.desafio.entity.Pessoas;

@Repository
public interface PessoaRepository extends MongoRepository<Pessoas, String> {
	Pessoas findBy_id(ObjectId _id);
}